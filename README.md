# Task
Build out the given design for both mobile and desktop version. The design is mentioned in the given GitLab repo whose link is : https://gitlab.com/mountblue/cohort-12-javascript-2/html-css-four-cards. 

# Notes
1. For desktop version, the background(that can seen in the given repo's desktop-preview.jpg file) should be visible whereas for mobile version, it should not
2. The design should look as close as possible to the given design 
3. For mobile version, the screen width is 375px, whereas, for desktop version, the screen width is 1440px.
